from unittest import mock
from stripe.error import InvalidRequestError, AuthenticationError
from gwx_payment.providers.stripe_service import StripeService
from tests.test_cases.payment_test_case import PaymentTestCase


class StripeServiceTest(PaymentTestCase):

    def test_get_payment_fail(self) -> None:
        """Assert that the retrieve request will return an InvalidRequestError exception,
        when an invalid request parameter is submitted.

        :return:None
        """

        payment_id = 'fake-id'
        with self.assertRaises(AuthenticationError) as context:
            StripeService(self.config).get_payment(payment_id)

    @mock.patch('stripe.PaymentIntent.retrieve')
    def test_get_payment_success(self, retrieve_mock) -> None:
        """Assert that the retrieve request will return a json result,
        containing the necessary details based on `get_payment_success_result` test case data.

        :param retrieve_mock: the mocked `stripe.PaymentIntent.retrieve` method
        :return:None
        """
        payment_id = 'authentic-id'
        result = self.get_test_case_data('stripe', 'get_payment_success_result')
        retrieve_mock.return_value = result
        payment = StripeService(self.config).get_payment(payment_id)

        self.assertEqual(payment, result)

    def test_confirm_payment_fail(self) -> None:
        """Assert that AuthenticationError is raised due to invalid request parameters being sent.

        :return:None
        """
        payment_id = 'invalid-payment-id'
        with self.assertRaises(AuthenticationError) as context:
            StripeService(self.config).confirm_payment(payment_id=payment_id, payment_method='invalid-payment-method')

            self.assertEqual(f'Invalid API Key provided: {self.config["api-key"]}', str(context))

    @mock.patch('stripe.PaymentIntent.confirm')
    def test_confirm_payment_success(self, confirm_mock) -> None:
        """Assert that return result after confirmation
        conforms to the implemented structure of the confirm_payment method.

        :return:None
        """
        payment_id = 'authentic-id'
        result = self.get_test_case_data('stripe', 'confirm_payment_success_result', True)
        confirm_mock.return_value = result.data

        mocked_result = {
            'code': 200,
            'message': 'succeeded',
            'data': result
        }
        payment = StripeService(self.config).confirm_payment(payment_id=payment_id, payment_method='card')

        self.assertEqual(payment, result.data)

    @mock.patch('stripe.PaymentIntent.create')
    def test_create_payment_success(self, create_mock) -> None:
        """Assert that return result of the create payment request
        conforms to the implemented structure of the create_payment method.

        :return:None
        """
        result = self.get_test_case_data('stripe', 'create_payment_success_result', True)
        create_mock.return_value = result.data

        payment = StripeService(self.config).create_payment(
            amount=2000,
            currency='aud',
            payment_method='card'
        )

        self.assertEqual(payment, result.data)

    def test_create_payment_fail(self) -> None:
        """Assert that AuthenticationError is raised due to invalid request parameters being sent.

        :return:None
        """
        with self.assertRaises(AuthenticationError) as context:
            StripeService(self.config).create_payment(
                amount=2000,
                currency='aud',
                payment_method='card'
            )

            self.assertEqual(f'Invalid API Key provided: {self.config["api-key"]}', str(context))
