import json
import os
from collections import namedtuple
from unittest import TestCase


class PaymentTestCase(TestCase):
    base_path = os.path.dirname(os.path.abspath(__file__))

    factory_path = f'{base_path}/factories'
    payment_data_path = f'{factory_path}/payment'
    service_config_path = f'{factory_path}/config'

    config = {'api_key': 'mock-api-key', 'public_key': 'mock-public-key'}

    def get_test_case_data(self, service: str, test_case_json_file: str, to_object=False):
        """Returns the requested factory file to be used for testing purposes.

        :param service: the payment provider service `name` being utilized for the payment module.
        :param test_case_json_file: the json file based on the test case being required.
        :param to_object False by default
        :return: dict if there is an existing test case, None if nothing found.
        """
        json_file = f'{self.payment_data_path}/{service}/{test_case_json_file}.json'

        try:
            with open(json_file, 'r') as file:
                if to_object is True:
                    return json.load(file, object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))
                return json.load(file)
        except FileExistsError:
            print(FileExistsError(f'Error: file {json_file} is not existing.'))
            return None
