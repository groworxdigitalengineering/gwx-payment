import pytest

from gwx_payment.abstract_payment_service import AbstractPaymentService
from gwx_payment.provider_service import ProviderService
from tests.test_cases.payment_test_case import PaymentTestCase


class ProviderServiceTest(PaymentTestCase):

    def test_get_payment_service(self):
        """
        Assert that the return type of get payment service is an instance of AbstractPaymentService.

        :return: None
        """
        self.assertIsInstance(
            ProviderService(self.service_config_path, 'payment.yaml').get_payment_service(),
            AbstractPaymentService
        )

    @pytest.mark.xfail
    def test_get_payment_service_fail(self):
        """
        Assert that the return type of get payment service is not an instance of ProviderService.

        :return: None
        """
        self.assertNotIsInstance(
            ProviderService(self.service_config_path, 'payment.yaml').get_payment_service(),
            ProviderService
        )
